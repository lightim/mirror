import SQLite from 'react-native-sqlite-storage';

var db = null;

const successcb = (_db) => {
    console.log('open database successfully');
    db = _db;
    db.executeSql(`CREATE TABLE IF NOT EXISTS friend_request(id INTEGER NOT NULL PRIMARY KEY, user_id INTEGER NOT NULL, avatar TEXT NOT NULL,nickname TEXT NOT NULL,message TEXT NOT NULL,ctime DATETIME NOT NULL,status integer NOT NULL DEFAULT 0)`).then((r) => {
        console.log('table friend_request created', r);
    });
    db.executeSql(`CREATE TABLE IF NOT EXISTS friend(id INTEGER NOT NULL PRIMARY KEY, nickname TEXT NOT NULL, avatar TEXT NOT NULL)`).then(() => {
        console.log('table friend created');
    });
    db.executeSql(`CREATE TABLE IF NOT EXISTS message(id TEXT NOT NULL PRIMARY KEY, version INTEGER NOT NULL, status INTEGER NOT NULL DEFAULT 0, fromUserId TEXT NOT NULL, toUserId TEXT NOT NULL, toGroupId TEXT NOT NULL, type INTEGER NOT NULL, contentMessage TEXT NOT NULL, ctime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP)`).then(() => {
        console.log('table message created');
    });
    db.executeSql(`CREATE TABLE IF NOT EXISTS session(type INTEGER NOT NULL, targetId TEXT NOT NULL, message TEXT NOT NULL,name TEXT NOT NULL,avatar TEXT NOT NULL, unread INTEGER NOT NULL DEFAULT 0,utime DATETIME NOT NULL,PRIMARY KEY(type,targetId))`).then(r => {
        console.log('table session created');
    });
}

const errorcb = (e) => {
    console.error('open database error', e);
}

SQLite.DEBUG(true);
SQLite.enablePromise(true);
SQLite.openDatabase({ name: 'mirror.db', location: 'default' }, successcb, errorcb);

export const DB = () => {
    return db;
}


export const ClearAll = () => {
    db.executeSql(`DELETE FROM friend_request`);
    db.executeSql(`DELETE FROM friend`);
    db.executeSql(`DELETE FROM message`);
    db.executeSql(`DELETE FROM session`);
}