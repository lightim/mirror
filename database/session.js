import { DB } from "./sqlite";




export const SessionType = {
    Person: 1,
    Group: 2,
};

export const InsertSession = (type, targetId, message, name, avatar, unread) => {
    const db = DB();

    return db.executeSql(`INSERT OR IGNORE INTO session(type,targetId,message,name,avatar,unread,utime) VALUES(?,?,?,?,?,0,CURRENT_TIMESTAMP)`, [type, targetId, message, name, avatar]).then(() => {
        return db.executeSql(`UPDATE session SET message=?,name=?,avatar=?,unread=unread+?,utime=CURRENT_TIMESTAMP WHERE type=? AND targetId=?`, [message, name, avatar, unread, type, targetId]);
    });
};

export const GetSessionUnread = (type, targetid) => {
    const db = DB();

    return db.executeSql(`SELECT unread FROM session WHERE type=? AND targetId=?`, [type, targetid]).then((rr) => {
        const r = rr[0];
        if (r.rows.length == 0) {
            return 0;
        }
        return r.rows.item(0).unread;
    });
}

export const FindSessions = () => {
    const db = DB();

    return db.executeSql(`SELECT * FROM session ORDER BY utime DESC`).then((rr) => {
        let results = [];
        const r = rr[0];
        for (let i = 0; i < r.rows.length; i++) {
            let row = r.rows.item(i);
            results.push(row);
        }
        return results;
    });
}

export const UpdateSessionUnread = (type, targetId, unread) => {
    const db = DB();

    return db.executeSql(`UPDATE session SET unread=? WHERE type=? AND targetId=?`, [unread, type, String(targetId)]).then(() => {
        console.log('UpdateSessionUnread', type, targetId, unread);
    });
};