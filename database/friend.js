import { DB } from './sqlite';

export const InsertFriendRequest = (id, user_id, avatar, nickname, message, ctime) => {
    const db = DB();
    db.executeSql(`INSERT OR IGNORE INTO friend_request(id,user_id,avatar,nickname,message,ctime) VALUES(?,?,?,?,?,?)`, [id, user_id, avatar, nickname, message, ctime]).then((r) => {
        console.log('insert friend request', r);
    });
}

export const FindFriendRequests = () => {
    const db = DB();
    return db.executeSql(`SELECT * FROM friend_request ORDER BY ctime DESC`).then((rr) => {
        let results = [];
        const r = rr[0];
        for (let i = 0; i < r.rows.length; i++) {
            let row = r.rows.item(i);
            results.push(row);
        }
        return results;
    });
};

export const UpdaetFriendRequestStatus = (id, status) => {
    const db = DB();
    return db.executeSql(`UPDATE friend_request SET status=? WHERE id=?`, [status, id]);
};

export const FindFriends = () => {
    const db = DB();
    return db.executeSql(`SELECT * FROM friend ORDER BY nickname`).then((rr) => {
        let results = [];
        const r = rr[0];
        for (let i = 0; i < r.rows.length; i++) {
            let row = r.rows.item(i);
            results.push(row);
        }
        return results;
    });
};

export const GetFriendById = (id) => {
    const db = DB();
    return db.executeSql(`SELECT * FROM friend WHERE id=?`, [parseInt(id),]).then((rr) => {
        const r = rr[0];
        if (r.rows.length > 0) {
            return r.rows.item(0);
        }
        return null;
    });
}

export const UpdateFriends = (friends) => {
    const db = DB();
    return db.executeSql(`DELETE FROM friend`).then((rr) => {
        let promises = [];
        for (let i = 0; i < friends.length; i++) {
            const f = friends[i];
            const p = db.executeSql(`INSERT OR IGNORE INTO friend(id,nickname,avatar) VALUES(?,?,?)`, [f.id, f.nickname, f.avatar]);
            promises.push(p);
        }
        return promises;
    });
}