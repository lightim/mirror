import Config from './config';
import { AsyncStorage } from 'react-native';


export const CreateFriendRequest = (userId, message) => {
    const url = Config.Host + '/friend/request';

    const data = {
        user_id: userId,
        message: message,
    };
    return AsyncStorage.getItem('token').then((token) => {
        return fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-Authorization': token,
            },
            body: JSON.stringify(data),
        }).then((response) => response.json());
    });

};

export const GetFriendRequest = (id) => {
    const url = Config.Host + '/friend/request/' + id;

    return AsyncStorage.getItem('token').then((token) => {
        return fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-Authorization': token,
            },
        }).then((response) => response.json());
    });
};

export const AcceptFriendRequest = (id) => {
    const url = Config.Host + '/friend/request/' + id;

    return AsyncStorage.getItem('token').then((token) => {
        return fetch(url, {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-Authorization': token,
            },
        }).then((response) => response.json());
    });
};

export const FetchUserFriends = () => {
    const url = Config.Host + '/friends';

    return AsyncStorage.getItem('token').then((token) => {
        return fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-Authorization': token,
            },
        }).then((response) => response.json());
    });
};