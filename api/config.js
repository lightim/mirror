

const Config = {
    Host: '',
    WSHost: '',
    DevHost: 'http://192.168.50.148:1212/api',
    ProdHost: 'http://laser.wikylyu.xyz:1212/api',
    DevWSHost: 'ws://192.168.50.148:11111/ws',
    ProdWSHost: 'ws://laser.wikylyu.xyz:11111/ws',
}

if (__DEV__) {
    Config.Host = Config.DevHost;
    Config.WSHost = Config.DevWSHost;
} else {
    Config.Host = Config.ProdHost;
    Config.WSHost = Config.ProdWSHost;
}

export default Config;