import Config from './config';


export const getSystem = () => {
    const url = Config.Host + '/system';
    return fetch(url, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    }).then((response) => response.json());
}