import Config from "./config";
import { AsyncStorage } from 'react-native';

export const Signup = (phone, password, nickname, avatar) => {
    const url = Config.Host + '/signup';

    const data = {
        phone,
        password,
        nickname,
        avatar,
    };

    return fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    }).then((response) => response.json());
};

export const Login = (phone, password) => {
    const url = Config.Host + '/login';

    const data = {
        phone,
        password,
    };
    return fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    }).then((response) => response.json());
};

export const SearchUsers = (s) => {
    const url = Config.Host + '/users?s=' + s;
    return AsyncStorage.getItem('token').then((token) => {
        return fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-Authorization': token,
            },
        });
    }).then((response) => response.json());

};

export const GetUser = (userId) => {
    const url = Config.Host + '/user/' + userId;
    const key = 'user.' + userId;
    return AsyncStorage.getItem('token').then((token) => {
        return fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-Authorization': token,
            },
        });
    }).then((response) => response.json()).then((body) => {
        if (body.data) {
            AsyncStorage.setItem(key, JSON.stringify(body.data));
        } else {
            throw 'Error';
        }
        return body.data || {};
    }).catch(() => {
        return AsyncStorage.getItem(key).then((a) => {
            if (!a) {
                return {};
            }
            return JSON.parse(a);
        });
    });
};

export const GetAccount = () => {
    const url = Config.Host + '/account';
    return AsyncStorage.getItem('token').then((token) => {
        return fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-Authorization': token,
            },
        });
    }).then((response) => response.json()).then(body => {
        if (body.data) {
            AsyncStorage.setItem('account', JSON.stringify(body.data));
        }
        return body.data || {};
    }).catch(() => {
        return AsyncStorage.getItem('account').then((a) => {
            if (!a) {
                return {};
            }
            return JSON.parse(a);
        });
    });
}