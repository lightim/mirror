import Config from './config';
import { getSystem } from './system';
import { AsyncStorage } from 'react-native';
import { Toast } from 'native-base'

var Version = 1;
var AppId = '';

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const guid = () => {
    return uuidv4() + parseInt(Math.random() * 123456);
}


getSystem().then((body) => {
    console.log(body);
    if (body && body.data && body.data.appId) {
        AppId = body.data.appId;
        AsyncStorage.setItem("appId", AppId);
    } else {
        throw 'Error'
    }
}).catch(() => {
    AsyncStorage.getItem("appId").then((appId) => {
        AppId = appId;
    });
})

export const MessageType = {
    Ping: 1,
    Pong: 2,

    AuthRequest: 1001,
    AuthResponse: 1002,
    ContentMessage: 1003,      // 私聊消息
    GroupContentMessage: 1004, // 群聊消息
    RoomContentMessage: 1005,
    MessageAck: 1006,
    Notification: 1007,
};

export const NotificationType = {
    Undefined: 0,
    KickOff: 1,
    FriendRequest: 2,
};

export const AuthResponseStatus = {
    OK: 1,
    AppNotFound: 2,
    TokenNotFound: 3,
};

export const NewWebSocket = () => {
    return new WebSocket(Config.WSHost);
};

export const AuthRequest = (token, client) => {
    return {
        version: Version,
        appId: AppId,
        type: MessageType.AuthRequest,
        authReq: {
            client: client,
            token: token,
        },
    };
}

export const MessageAck = (mid) => {
    return {
        id: guid(),
        version: Version,
        appId: AppId,
        type: MessageType.MessageAck,
        messageAck: {
            msgId: mid,
        },
    };
}

export const ContentMessageType = {
    Text: 1,
    Image: 2,
    Voice: 3,
    Video: 4,
    File: 5,
};

export const ContentMessage = (uid, type, content) => {
    return {
        id: guid(),
        version: Version,
        appId: AppId,
        toUserId: String(uid),
        type: MessageType.ContentMessage,
        contentMessage: {
            type: type,
            content: content,
        },
    }
};

const WebSocketState = {
    UNCONNECTED: 0,
    CONNECTED: 1,
    AUTHED: 2,
}

var wsState = WebSocketState.UNCONNECTED;
var ws = null;


const onopen = () => {
    console.log('onopen');
    wsState = WebSocketState.CONNECTED;
    AsyncStorage.getItem('token').then((token) => {
        SendMessage(AuthRequest(token, ''));
    });
};

const onclose = (e) => {
    if (e.target !== ws) {
        return;
    }
    console.log('onclose');
    wsState = WebSocketState.UNCONNECTED;
    ws.onclose = null;
    ws = null;
    setTimeout(ReconnectWebSocket, 5000);
};

var messagecbs = [];

export const RegisterOnMessage = (f) => {
    messagecbs.push(f);
};

export const CancelOnMessage = (f) => {
    let tmp = [];
    for (let i = 0; i < messagecbs.length; i++) {
        if (messagecbs[i] === f) {
            continue;
        }
        tmp.push(messagecbs[i]);
    }
    messagecbs = tmp;
}

const onmessage = (e) => {
    const m = JSON.parse(e.data);
    console.log('onmessage', m);
    if (wsState === WebSocketState.CONNECTED) {
        if (m.type !== MessageType.AuthResponse) {
            console.log('unexpected message');
            onclose();
            return;
        }
        if (m.authResp.status !== AuthResponseStatus.OK) {
            Toast.show({
                text: '连接聊天服务器失败，请重新登录',
                buttonText: '确定',
                duration: 0,
            });
            wsState = WebSocketState.UNCONNECTED;
            return;
        }
        wsState = WebSocketState.AUTHED;
        console.log('authed');
        return;
    };

    if (messagecbs.length > 0) {
        messagecbs[messagecbs.length - 1](m);
    }
}

export const ReconnectWebSocket = () => {
    console.log('reconnect', wsState);
    if (wsState !== WebSocketState.UNCONNECTED) {
        return;
    }
    ws = NewWebSocket();
    ws.onopen = onopen;
    ws.onclose = onclose;
    ws.onerror = onclose;
    ws.onmessage = onmessage;
}

export const CloseWebSocket = () => {
    if (!ws) {
        return;
    }
    wsState = WebSocketState.UNCONNECTED;
    ws.onclose = null;
    ws.close();
    ws = null;
};

export const SendMessage = (data) => {
    if (!ws) {
        return false;
    }
    console.log('sendMessage', data);
    ws.send(JSON.stringify(data));
    return true;
};