import React from 'react';
import { View, StatusBar, Text, AsyncStorage, StyleSheet } from 'react-native';
import { Button, List, ListItem } from 'native-base';
import { ClearAll } from '../database/sqlite';
import { StackActions, NavigationActions, NavigationEvents } from 'react-navigation';
import { CloseWebSocket } from '../api/ws';


class SettingScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            title: '设置',
            headerStyle: {
                backgroundColor: '#222',
            },
            headerTintColor: '#fff',
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#222" barStyle="light-content" />
                <View style={styles.content}>
                    <List style={styles.list}>
                        <ListItem style={[styles.listItem, { borderBottomWidth: 1 }]}>
                            <Text>新消息提醒</Text>
                        </ListItem>
                        <ListItem style={[styles.listItem, { borderBottomWidth: 1 }]}>
                            <Text>勿扰模式</Text>
                        </ListItem>
                        <ListItem style={[styles.listItem, { borderBottomWidth: 1 }]}>
                            <Text>聊天</Text>
                        </ListItem>
                        <ListItem style={[styles.listItem, { borderBottomWidth: 1 }]}>
                            <Text>隐私</Text>
                        </ListItem>
                        <ListItem style={[styles.listItem, { borderBottomWidth: 0 }]}>
                            <Text>通用</Text>
                        </ListItem>
                    </List>

                    <List style={[styles.list, { marginTop: 20 }]}>
                        <ListItem style={[styles.listItem, { borderBottomWidth: 1 }]}>
                            <Text>关于镜像</Text>
                        </ListItem>
                        <ListItem style={[styles.listItem, { borderBottomWidth: 0 }]}>
                            <Text>帮助与反馈</Text>
                        </ListItem>
                    </List>

                    <List style={[styles.list, { marginTop: 20 }]}>
                        <ListItem style={[styles.listItem, { borderBottomWidth: 0 }]}>
                            <Text>插件</Text>
                        </ListItem>
                    </List>

                    <List style={[styles.list, { marginTop: 20 }]}>
                        <ListItem style={[styles.listItem, { borderBottomWidth: 1 }]}>
                            <Text>切换帐号</Text>
                        </ListItem>
                        <ListItem onPress={this.onLogout} style={[styles.listItem, { borderBottomWidth: 0 }]}>
                            <Text>退出</Text>
                        </ListItem>
                    </List>
                </View>
            </View>
        );
    }

    onLogout = () => {
        AsyncStorage.clear();
        ClearAll();
        CloseWebSocket();
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Welcome' })],
        });

        this.props.navigation.dispatch(resetAction);
    }
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        width: '100%',
        height: '100%',
    },
    content: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
    },
    list: {
        backgroundColor: 'white',
        paddingRight: 18,
    },
    listItem: {
        height: 50,
        borderBottomColor: '#EEE',
    }
});

export default SettingScreen;