import React from 'react';
import { ImageBackground, StatusBar, View, StyleSheet, AsyncStorage } from 'react-native';
import { StackActions } from 'react-navigation';

class WelcomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  render() {
    return (
      <View>
        <StatusBar backgroundColor="#000" />
        <ImageBackground source={require('../asset/img/startup.jpeg')} style={styles.background}>
        </ImageBackground>
      </View>
    );
  }

  componentDidMount() {
    this.timer = setTimeout(this.goto, 1500);
  }
  componentWillUnmount() {
    this.timer && clearTimeout(this.timer);
  }

  goto = () => {
    AsyncStorage.getItem('token').then((token) => {
      let routeName = 'Auth';
      if (token) {
        routeName = 'Home';
      }

      const navigateAction = StackActions.replace({
        routeName: routeName,
      });

      this.props.navigation.dispatch(navigateAction);
    });
  }
}

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
  },
})

export default WelcomeScreen;
