import React from 'react';
import { View, StatusBar, Text, StyleSheet } from 'react-native';
import { Button, Input, Item, Switch, Toast } from 'native-base';
import { Row } from 'react-native-easy-grid';
import { GetAccount } from '../api/account';
import { CreateFriendRequest } from '../api/friend';



class AddFriendScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            title: '验证申请',
            headerStyle: {
                backgroundColor: '#222',
            },
            headerTintColor: '#fff',
            headerRight: (<View><Button onPress={params.onSend} transparent style={{ marginRight: 20 }}><Text style={{ color: '#04be01', fontSize: 18 }}>发送</Text></Button></View>)
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            self: {},
            user: this.props.navigation.getParam('user', {}),
            message: '我是',
            blocked: false,
        };
        this.unmounted = false;
        GetAccount().then((a) => {
            if (this.unmounted) {
                return;
            }
            this.setState({ self: a, message: '我是' + a.nickname });
        });
    }

    componentDidMount() {
        this.props.navigation.setParams({ onSend: this.onSend });
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    render() {
        const { user, message, blocked } = this.state;
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#222" barStyle="light-content" />
                <Row style={[styles.row, { paddingTop: 12 }]}>
                    <Text>你需要发送验证申请，等对方通过</Text>
                </Row>
                <Row style={[styles.row, { paddingBottom: 12 }]}>
                    <Item success style={{ width: '100%' }}>
                        <Input value={message} onChangeText={(message) => this.setState({ message })} ></Input>
                    </Item>
                </Row>

                <Row style={[styles.row, { paddingTop: 12, marginTop: 12 }]}>
                    <Text style={styles.grayText}>为朋友设置备注</Text>
                </Row>
                <Row style={[styles.row, { paddingBottom: 12 }]}>
                    <Item disabled style={{ width: '100%' }}>
                        <Input style={styles.grayText} disabled value={user.nickname}></Input>
                    </Item>
                </Row>

                <Row style={[styles.row, { paddingTop: 12, marginTop: 12 }]}>
                    <Text style={styles.grayText}>设置朋友圈权限</Text>
                </Row>
                <Row style={[styles.row, { paddingBottom: 12, paddingTop: 12 }]}>
                    <Text style={{ flex: 1 }}>不让他(她)看我的朋友圈</Text>
                    <Switch value={blocked} onValueChange={(blocked) => this.setState({ blocked })}></Switch>
                </Row>
            </View >
        )
    }

    onSend = () => {
        let { user, message } = this.state;
        CreateFriendRequest(user.id, message).then((body) => {
            console.log(body);
            if (body.status !== 0) {
                throw 'Error';
            }
            this.props.navigation.goBack();
        }).catch(() => {
            Toast.show({
                text: '发送失败',
                duration: 3000,
            });
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        height: '100%',
        display: 'flex',
    },
    row: {
        backgroundColor: 'white',
        paddingLeft: 12,
        paddingRight: 12,
        flex: 0,
        flexDirection: 'row',
    },
    grayText: {
        color: 'gray',
    },
});

export default AddFriendScreen;