import React from 'react';
import { View, Text, StatusBar, StyleSheet, AsyncStorage } from 'react-native';
import { Button, List, Row, Input, Item, Icon, ListItem, Thumbnail, Left, Right, Body } from 'native-base';
import { FindFriendRequests, UpdaetFriendRequestStatus } from '../database/friend';
import { AcceptFriendRequest } from '../api/friend';


class NewFriendScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            title: '新的朋友',
            headerStyle: {
                backgroundColor: '#222',
            },
            headerTintColor: '#fff',
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            requests: [],
        };
        FindFriendRequests().then((r) => {
            this.setState({ requests: r })
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#222" barStyle="light-content" />
                <Row style={[styles.row, { marginTop: 14 }]}>
                    <Item style={{ flexDirection: 'row', width: '100%', borderBottomWidth: 0 }}>
                        <Icon active name='ios-search' />
                        <Input disabled style={{ fontSize: 14 }} placeholder='镜像号/手机号' />
                    </Item>
                </Row>
                <Text style={{ marginLeft: 12, marginTop: 14, marginBottom: 4 }}>新的朋友</Text>
                <List style={{ backgroundColor: 'white' }} dataArray={this.state.requests}
                    renderRow={(item) =>
                        <ListItem thumbnail onLongPress={() => this.onItemLongPress(item)} onPress={() => this.onItemPress(item)}>
                            <Left>
                                <Thumbnail square source={{ uri: item.avatar }} />
                            </Left>
                            <Body>
                                <Text>{item.nickname}</Text>
                                <Text note numberOfLines={1}>{item.message}</Text>
                            </Body>
                            <Right>
                                {item.status === 0 ? (<Button onPress={() => this.onAcceptRequest(item)} success style={{ paddingLeft: 10, paddingRight: 10 }}><Text style={{ color: 'white', fontSize: 14 }}>接受</Text></Button>) : (<Text>已处理</Text>)}
                            </Right>
                        </ListItem>
                    }>
                </List>
            </View >
        );
    }

    componentWillMount() {
        AsyncStorage.setItem('newFriend', '0');
    }

    onAcceptRequest = (item) => {
        AcceptFriendRequest(item.id).then((body) => {
            if (body.status === 0) {
                UpdaetFriendRequestStatus(item.id, 1);
                FindFriendRequests().then((r) => {
                    this.setState({ requests: r })
                });
            }
        });
    }
    onItemLongPress = (item) => {
        alert(2);
    }
    onItemPress = (item) => {
        const u = {
            id: item.user_id,
            avatar: item.avatar,
            nickname: item.nickname,
        }
        this.props.navigation.navigate({ routeName: 'User', params: { user: u } });
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        height: '100%',
        display: 'flex',
    },
    row: {
        width: '100%',
        backgroundColor: 'white',
        paddingLeft: 12,
        paddingRight: 12,
        flex: 0,
        flexDirection: 'row',
    },
    grayText: {
        color: 'gray',
    },
});



export default NewFriendScreen